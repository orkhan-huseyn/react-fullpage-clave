import React from "react";
import Cleave from "cleave.js/react";

import "cleave.js/dist/addons/cleave-phone.az";

import "./PersonalInfoForm.css";

export default function PersonalInfoForm({ fullPageRef }) {
  function handleNextClick() {
    fullPageRef.current.moveSectionDown();
  }

  return (
    <div className="personal-info-form">
      <Cleave id="firstName" placeholder="Enter your first name" />
      <Cleave id="lastName" placeholder="Enter your last name" />
      <Cleave
        id="phoneNumber"
        placeholder="Enter your phone number"
        options={{ phone: true, phoneRegionCode: "AZ" }}
      />
      <button id="nextButton" onClick={handleNextClick}>
        Next
      </button>
    </div>
  );
}
