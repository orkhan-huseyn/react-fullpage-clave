import React, { useEffect, useRef } from "react";
import fullpage from "fullpage.js";

import PersonalInfoForm from "./PersonalInfoForm";
import CreditCardForm from "./CreditCardForm";

import "./App.css";

export default function App() {
  const fullPageRef = useRef(null);

  useEffect(() => {
    fullPageRef.current = new fullpage("#fullpage", {
      sectionsColor: ["#ff5f45", "#0798ec"],
    });

    return () => {
      fullPageRef.current.destroy();
    };
  }, []);

  return (
    <div id="fullpage">
      <div className="section active">
        <div className="form-wrapper">
          <h1>Your personal information</h1>
          <PersonalInfoForm fullPageRef={fullPageRef} />
        </div>
      </div>
      <div className="section">
        <div className="form-wrapper">
          <h1>Your payment information</h1>
          <CreditCardForm fullPageRef={fullPageRef} />
        </div>
      </div>
    </div>
  );
}
